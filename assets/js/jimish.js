/*-----------------------------------------------------------
 * Author           : Jimish Gajjar 
 * Version          : 1.0.0
 * File Description : Main js file of the template
 *------------------------------------------------------------
 */

 document.onreadystatechange = function () {
    if (document.readyState !== "complete") {
        document.querySelector(
            "body").style.visibility = "hidden";
        document.querySelector("#loader").style.visibility = "visible";
        // var str = "We are very very small, but we are profoundly capable of <b>Very big things</b>";
        // var spans = '<span>' + str.split(/\s+/).join(' </span><span>') + '</span>';
        var spans = '<span class="loading-text tiempos" style="margin-left:-35px; margin-top: 100px;">"The </span>' +
            '<span class="loading-text tiempos">sky </span>' +
            '<span class="loading-text tiempos">is </span>' +
            '<span class="loading-text tiempos">not </span>' +
            '<span class="loading-text tiempos">the </span>' +
            '<span class="loading-text tiempos">limit; </span><br>' +
            '<span class="loading-text tiempos" style="margin-left:-15px;">it </span>' +
            '<span class="loading-text tiempos">is </span>' +
            '<span class="loading-text tiempos">just </span>' +
            '<span class="loading-text tiempos">the </span>' +
            '<span class="loading-text tiempos">beginning </span>' +
            '<span class="loading-text tiempos">of </span>' +
            '<span class="loading-text tiempos">infinite </span>' +
            '<span class="loading-text montserrat1" > </span>' +   
            '<p class="loading-text montserrat1" style="margin-left:-15px;">possibilities."</p>' +
            '<p class="tiempos sthaw"><b> - Nebula Logix</b></p>';
        $(spans).hide().appendTo('#loader').each(function (i) {
            $(this).delay(500 * i).fadeIn();
        });
    }
    setTimeout(
        function () {
            document.querySelector("#loader").style.display = "none";
            document.querySelector("body").style.visibility = "visible";
            document.querySelector("#down-errow").style.visibility = "visible";
            // $('.content[data-position="1"]').addClass('active');
        }, 9000
    )
};

// repeated variables
var $window = $(window);
var $root = $("html, body");

$(document).ready(function () {
    "use strict";

    menuToggler();
    $(".owl-item.active .hero-slide").addClass("zoom");
});

$window.on("load", function () {
    history.pushState(null, "", location.href.split("#")[0]);

    $("#overlayer").delay(500).fadeOut("slow");
    $(".loader").delay(1000).fadeOut("slow");
    pagePilling();
    // portfolioIsotop();
});

$(".to-contact").on("click", function () {
    $("section.active").removeClass("active");
    var $id = $(this).attr("href");
    $("#main").children($id).addClass("active");
});



/*-----------------------------------------------------------------------------
                                   FUNCTIONS
-----------------------------------------------------------------------------*/
/*-------------------------
       Page Pilling
-------------------------*/
function pagePilling() {
    "use strict";

    var ids = [];
    var tooltips = [];
    var colors = [];
    $(".section").each(function () {
        ids.push(this.id);
        tooltips.push($(this).data("navigation-tooltip"));
        colors.push($(this).data("navigation-color"));
    });
    $("#pagepiling").pagepiling({
        sectionsColor: colors,
        anchors: ids,
        menu: "#myMenu",
        direction: "vertical",
        verticalCentered: true,
        navigation: {
            position: "right",
            tooltips: tooltips,
        },
        loopBottom: true,
        loopTop: true,
        scrollingSpeed: 700,
        easing: "swing",
        css3: true,
        normalScrollElements: ".owl-stage-outer",
        normalScrollElementTouchThreshold: 5,
        touchSensitivity: 5,
        keyboardScrolling: true,
        sectionSelector: ".section",
        animateAnchor: true,
        //events
        onLeave: function (index, nextIndex, direction) {
            if (nextIndex == 1) {
                $(".special-section").css("color", "#fff");
            } else {
                $(".special-section").css("color", "#3c3c3c");
            }

            if (nextIndex == 1 && $(".section.hero").hasClass("speacial-hero")) {
                $("#pp-nav li span").css("backgroundColor", "#fff");
            } else {
                $("#pp-nav li span").css("backgroundColor", "#3c3c3c");
            }

            if (nextIndex == 1 && $(".section.hero").hasClass("speacial-hero")) {
                $("#pp-nav li .pp-tooltip").css("color", "#fff");
            } else {
                $("#pp-nav li .pp-tooltip").css("color", "#3c3c3c");
            }
        },
        afterLoad: function (anchorLink, index) {},
        afterRender: function (index) {
            if (index > 1) {
                $(".special-section").css("color", "#3c3c3c");
            } else {
                $(".special-section").css("color", "#fff");
            }

            if (index > 1 && $(".section.hero").hasClass("speacial-hero")) {
                $("#pp-nav li span").css("backgroundColor", "#3c3c3c");
            } else if ($(".section.hero").hasClass("speacial-hero")) {
                $("#pp-nav li span").css("backgroundColor", "#fff");
            }

            if (index > 1 && $(".section.hero").hasClass("speacial-hero")) {
                $("#pp-nav li .pp-tooltip").css("color", "#3c3c3c");
            } else if ($(".section.hero").hasClass("speacial-hero")) {
                $("#pp-nav li .pp-tooltip").css("color", "#fff");
            }
        },
    });
}

function submitToAPI(e){
    e.preventDefault();
// var URL = "https://1t4yx7grhe.execute-api.us-west-2.amazonaws.com/prod/contact";
    var name = document.getElementById("name-input").value;
    var email = document.getElementById("email-input").value;
    var desc = document.getElementById("message-input").value;
    var msgElement = document.getElementById("returnMsg");
    var data = {
        email : email,
        name : name,    
        message : desc
      };
      console.log(data);
      //https://sendmessaginfnebula.onrender.com/user/envoiMessage
    //   http://localhost:4000/user/envoiMessage
    fetch("https://mail-api.americaa.de/user/envoiMessage",
    {
        method:"POST",
        headers: {
            'Accept':'application/json',
            'Content-Type':'application/json'
        },
        body:JSON.stringify(data)
    }
    )
    .then(response=> response.json())
    .then(data=>console.log(data))
    .catch((err)=>console.log(err))

    
    msgElement.classList.remove("hiddennMsg");
    msgElement.classList.remove("errorMsg");
    msgElement.innerHTML = "Contacting...";
    msgElement.classList.remove("successMsg");

    if (name=="" || email=="" || desc=="")
     { 
        msgElement.classList.add("errorMsg");  
        msgElement.innerHTML = "Please Fill All Required Field";
         //alert("Please Fill All Required Field");
         return false;
     }
     
     //nameRE = /^[A-Z]{1}[a-z]{2,20}[ ]{1}[A-Z]{1}[a-z]{2,20}/;
	 nameRE=/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
     if(!nameRE.test(name)) {
         //alert("Name entered, is not valid");
         msgElement.classList.add("errorMsg"); 
         msgElement.innerHTML = "Name entered, is not valid"; 
             return false;
     }
     
     
     emailRE = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
     if(!emailRE.test(email)) {
         //alert("Email Address entered, is not valid");
         msgElement.classList.add("errorMsg"); 
         msgElement.innerHTML = "Email Address entered, is not valid";  
             return false;
     }
    var data = {
       email : email,
       name : name,
       desc : desc
     };

     $.ajax({
      type: "POST",
      url : "https://mail-api.americaa.de/user/envoiMessage",
       // url:"https://3pukbar4bsyqicsl7thzeghrt40aruko.lambda-url.us-east-2.on.aws/",
      dataType: "json",
      crossDomain: "true",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(data),


      success: function () {
        // clear form and show a success message
        msgElement.classList.add("successMsg");  
        msgElement.innerHTML = "Thank you for your interest.";  
        document.getElementById("contactForm").reset();
    //location.reload();
      },
      error: function () {
        // show an error message
        msgElement.innerHTML = "Error : Please contact site administrator";  
        msgElement.classList.add("errorMsg"); 
      }}); 
 
 } 

/*-------------------------
    MENU TOGGLER
-------------------------*/
function menuToggler() {
    "use strict";

    $(".overlay-menu-toggler").on("click", function () {
        $(".overlay-menu").addClass("show");
    });
    $(".overlay-menu").on("click", function () {
        $(this).removeClass("show");
    });
}